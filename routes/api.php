<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\categorieController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/t', [categorieController::class, 'index1']);
Route::post('/cat', [categorieController::class, 'create']);
Route::delete('/del/{id}', [categorieController::class, 'deleteCat']);
Route::put('/update/{id}', [categorieController::class, 'update']);

