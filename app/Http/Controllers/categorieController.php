<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class categorieController extends Controller
{
    public function index(){
        try {
            $categories = DB::table('categories')->get();

            return view('views.index', ['categories' => $categories]);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public function index1(){
        try {
            $categories = DB::table('categories')->get();

            return $categories;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
  
    public function create(Request $request){
        try {
            DB::table('categories')->insert([
                'libelle' => $request->lib
            ]);
             
           
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
   
    public function deleteCat($id){
        try {
            DB::table('categories')->where('idc', '=', $id)->delete();
             
           
        } catch (\Throwable $th) {
            

            return $th->getMessage();
        }
    }
    public function update($id,Request $request){
        try {
            DB::table('categories')->where('idc', '=', $id)->update([
                'idc'=>$request->id,
                'libelle' => $request->lib
                
            ]);
             
           
        } catch (\Throwable $th) {
            
            
            return $th->getMessage();
        }
    }
}
